const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const http = require('http');

const app = express();
let server;

server =  http.createServer(app);
server.listen(process.env.PORT);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// importing routes
const phoneNumberRoutes = require('./routes/phoneNumber');
phoneNumberRoutes(app);

app.get('/', function(req, res) {
  res.send('Welcome to the october Test API');
});
console.log('API server started on: ' + process.env.PORT);

module.exports = app;
