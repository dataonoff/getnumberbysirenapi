let currentSocieteInfoApiKey = '1nvrvjes5dhp4qno19e2gb3r4o8ibq0lggvsk85633bcu7asish9';
const axios = require('axios');
const _get = require('lodash/get');


exports.getCompanyInfo = function(req, res) {
  console.log(req.params.siren);
  let requestWithSirenUrl = `https://societeinfo.com/app/rest/api/v2/company.json/${req.params.siren}?key=${currentSocieteInfoApiKey}`;
  let requestWithNameUrl = `https://societeinfo.com/app/rest/api/v2/company.json?name=${req.params.company}&key=${currentSocieteInfoApiKey}`;
  axios.get(req.params.siren ? requestWithSirenUrl : requestWithNameUrl)
    .then(response => {
      let phoneResult = _get(response.data, 'result.contacts.phones', []);
      if (phoneResult.length) {
        res.send({phoneNumber: phoneResult[0].value});
      } else {
        res.send(`Sorry, no phone number found for ${req.params.company}`);
      }
    }).catch(error => {
    res.send(`${error.message} ${(error.response && error.response.statusText) ? error.response.statusText : ''}`);
  });
};
