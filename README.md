# GetCompanyNumberByNameOrSiren

GetCompanyNumberByNameOrSiren est un projet de test technique pour October.

Le but étant de créer une API qui prend en paramètre le nom d'une société et son numéro de Siren (non obligatoire). L'api renvoie en json un numéro de téléphone en cas succès.

Ce projet utilise node.js.

ex: `http://localhost:3000/number/EXPERDECO/303830244`
should returns: `{"phoneNumber":"04 50 34 63 54"}`

Le numéro de Siren est fortement recommandé pour améliorer le résultat.

L'API utilise une API externe (limité à 100 requêtes):
`societeinfo.com` 

## Development server (local env)


Avant de lancer l'application pour la première fois, faire un `npm install` à la racine du projet afin de télécharger les dépendances.
Run `npm run app` for a dev server. 
Navigate to `http://localhost:300/`. 

