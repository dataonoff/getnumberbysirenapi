module.exports = function (app) {
  const externalApi = require('../apiService/externalApi');

  app.route('/number/:company/:siren?')
    .get(externalApi.getCompanyInfo);

};
